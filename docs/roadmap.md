# Governance
Over its first year of existence, the iFDO format was developed by members of the MareHub working group on Images/Videos and through the HGF/INF/HMC-funded project FDO-5DI of DLR and GEOMAR. Additional input was collected from the international marine imaging community. As the next step, a steering board of up to ten members from academia, industry and governance and with international representation shall be assembled at the upcoming Marine Imaging Workshop (10/2022). Please write an e-mail to tschoening@geomar.de in case you are interested to join the steering board. The steering board will then create a statute for the future development of the iFDO format over half a year (ca. 04/2023).

# Roadmap of iFDO development

## Upcoming versions

### v2.0.0
Should include:
- provenance mechanism
- unit names in field names for lat,lon,depth,... (TBD!)
- stereo imagery
- possibility to encode parts of the iFDO file in binary

### v1.2.0
Additional fields to reference information from iFDOs in external resources (e.g. equipment database).

## Current version v1.1.0
Updated fields and more detailed, consistent and specific documentation.

## Previous versions

### v1.0.0
Initial publication of the iFDO concept through the repository at https://gitlab.hzdr.de/datahub/marehub/ag-videosimages/fair-marine-images and with static supplementary material published as OceanBest Practice (https://hdl.handle.net/11329/1781 and https://hdl.handle.net/11329/1782).
