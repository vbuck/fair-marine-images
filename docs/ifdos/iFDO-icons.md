# Visualizing iFDO information with icons
Some of the iFDO fields require using well-defined values for those fields as described in the tables on the specific iFDO sections. For these well-defined values, icons were created to enable visualizing this capture information in web interfaces or data processing reports. These icons are available in the ressources folder and an overview is given here. You are free to use these icons. They are released publicly under CC-0.

## iFDO core icons
| Field | Value | Icon | Description |
| ----- | ----- | ---- | ----------- |
| image-license | CC-BY | ![license:CC-BY](svgs/image-license_CC-BY.svg "license:CC-BY") | You are free to share and adapt the images as long as you give appropriate credit, link to the license, indicate your changes. |
|  | CC-0 | ![license:CC-0](svgs/image-license_CC-0.svg "license:CC-0") | You can copy, modify, distribute and perform the work, even for commercial purposes, all without asking permission |

## iFDO capture icons
| Field | Value | Icon | Description |
| ----- | ----- | ---- | ----------- |
| image-acquisition | slide | ![acquisition:slide](svgs/image-acquisition_slide.svg "acquisition:slide") | The image set contains microscopy images / slide scans |
|  | video | ![acquisition:video](svgs/image-acquisition_video.svg "acquisition:video") | The image set contains moving images |
|  | photo | ![acquisition:photo](svgs/image-acquisition_photo.svg "acquisition:photo") | The image set contains still images |
| image-quality | raw | ![quality:raw](svgs/image-quality_raw.svg "quality:raw") | The images in the image set come straight from the sensor |
|  | product | ![quality:product](svgs/image-quality_product.svg "quality:product") | The images in the image set are ready for interpretation |
|  | processed | ![quality:processed](svgs/image-quality_processed.svg "quality:processed") | The images in the image set have been QA/QCd |
| image-deployment | exploration | ![deployment:exploration](svgs/image-deployment_exploration.svg "deployment:exploration") | The camera followed an unplanned path execution |
|  | sampling | ![deployment:sampling](svgs/image-deployment_sampling.svg "deployment:sampling") | The camera imaging samples taken by other method ex-situ |
|  | stationary | ![deployment:stationary](svgs/image-deployment_stationary.svg "deployment:stationary") | The camera remaind in a fixed spatial position |
|  | survey | ![deployment:survey](svgs/image-deployment_survey.svg "deployment:survey") | The camera followed a planned path execution along a free path |
|  | experiment | ![deployment:experiment](svgs/image-deployment_experiment.svg "deployment:experiment") | The camera observed a manipulation of the environment |
|  | mapping | ![deployment:mapping](svgs/image-deployment_mapping.svg "deployment:mapping") | The camera followed a planned path execution along 2-3 spatial axes |
| image-navigation | beacon | ![navigation:beacon](svgs/image-navigation_beacon.svg "navigation:beacon") | Position data was created from underwater beacons (USBL, ...) for an underwater position |
|  | satellite | ![navigation:satellite](svgs/image-navigation_satellite.svg "navigation:satellite") | Position data was created from satellite information (GPS, Galileo, ...) for the sea surface |
|  | transponder | ![navigation:transponder](svgs/image-navigation_transponder.svg "navigation:transponder") | Position data was created from underwater beacons (USBL, LBL, ...) for an underwater position |
|  | reconstructed | ![navigation:reconstructed](svgs/image-navigation_reconstructed.svg "navigation:reconstructed") | Position data was estimated from other measures like cable length and course over ground |
| image-scale-reference | calibrated camera | ![scale-reference:calibrated camera](svgs/image-scale-reference_calibrated camera.svg "scale-reference:calibrated camera") | Meter-scale in the images was determined by image data and additional external data like object distance |
|  | laser marker | ![scale-reference:laser marker](svgs/image-scale-reference_laser marker.svg "scale-reference:laser marker") | Meter-scale in the images was determined by laser markers visible in the images |
|  | optical flow | ![scale-reference:optical flow](svgs/image-scale-reference_optical flow.svg "scale-reference:optical flow") | Meter-scale in the images was determined from the relative movement of the images and the camera navigation data |
|  | 3D camera | ![scale-reference:3D camera](svgs/image-scale-reference_3D camera.svg "scale-reference:3D camera") | Meter-scale in the images was determined by 3D imaging / reconstruction |
| image-illumination | artificial | ![illumination:artificial](svgs/image-illumination_artificial.svg "illumination:artificial") | The scene is only illuminated by artificial light |
|  | sun | ![illumination:sun](svgs/image-illumination_sun.svg "illumination:sun") | The scene is only illuminated by the sun |
|  | mixed | ![illumination:mixed](svgs/image-illumination_mixed.svg "illumination:mixed") | The scene is illuminated by both sunlight and artificial light |
| image-resolution | dm | ![resolution:dm](svgs/image-resolution_dm.svg "resolution:dm") | The average size of a pixel in the image set is on the order of 1 dm = 0.1 m |
|  | hm | ![resolution:hm](svgs/image-resolution_hm.svg "resolution:hm") | The average size of a pixel in the image set is on the order of 1 hm = 100 m |
|  | m | ![resolution:m](svgs/image-resolution_m.svg "resolution:m") | The average size of a pixel in the image set is on the order of 1 m |
|  | cm | ![resolution:cm](svgs/image-resolution_cm.svg "resolution:cm") | The average size of a pixel in the image set is on the order of 1 cm |
|  | dam | ![resolution:dam](svgs/image-resolution_dam.svg "resolution:dam") | The average size of a pixel in the image set is on the order of 1 dam = 10 m |
|  | μm | ![resolution:μm](svgs/image-resolution_μm.svg "resolution:μm") | The average size of a pixel in the image set is on the order of 1 µm |
|  | mm | ![resolution:mm](svgs/image-resolution_mm.svg "resolution:mm") | The average size of a pixel in the image set is on the order of 1 mm |
|  | km | ![resolution:km](svgs/image-resolution_km.svg "resolution:km") | The average size of a pixel in the image set is on the order of 1 km |
| image-marine-zone | watercolumn | ![marine-zone:watercolumn](svgs/image-marine-zone_watercolumn.svg "marine-zone:watercolumn") | The images were taken in the water column without the seafloor or the sea surface in sight |
|  | atmosphere | ![marine-zone:atmosphere](svgs/image-marine-zone_atmosphere.svg "marine-zone:atmosphere") | The images were taken outside of the water |
|  | seasurface | ![marine-zone:seasurface](svgs/image-marine-zone_seasurface.svg "marine-zone:seasurface") | The images were taken right below the sea surface |
|  | laboratory | ![marine-zone:laboratory](svgs/image-marine-zone_laboratory.svg "marine-zone:laboratory") | The images were taken ex-situ |
|  | seafloor | ![marine-zone:seafloor](svgs/image-marine-zone_seafloor.svg "marine-zone:seafloor") | The images were taken in/on/right above the seafloor |
| image-spectral-resolution | grayscale | ![spectral-resolution:grayscale](svgs/image-spectral-resolution_grayscale.svg "spectral-resolution:grayscale") | The images consist of one color channel |
|  | multi-spectral | ![spectral-resolution:multi-spectral](svgs/image-spectral-resolution_multi-spectral.svg "spectral-resolution:multi-spectral") | The images consist of 4-10 color channels |
|  | rgb | ![spectral-resolution:rgb](svgs/image-spectral-resolution_rgb.svg "spectral-resolution:rgb") | The images consist of three color channels |
|  | hyper-spectral | ![spectral-resolution:hyper-spectral](svgs/image-spectral-resolution_hyper-spectral.svg "spectral-resolution:hyper-spectral") | The images consist of more than 10 channels |
| image-capture-mode | timer-and-manual | ![capture-mode:timer-and-manual](svgs/image-capture-mode_timer-and-manual.svg "capture-mode:timer-and-manual") | Image-acquisition was triggered by both a timer and a human-controller |
|  | manual | ![capture-mode:manual](svgs/image-capture-mode_manual.svg "capture-mode:manual") | Image-acquisition was triggered by a human controller |
|  | timer | ![capture-mode:timer](svgs/image-capture-mode_timer.svg "capture-mode:timer") | Image-acquisition was triggered by a timer |

## iFDO content icons
| Field | Value | Icon | Description |
| ----- | ----- | ---- | ----------- |
| image-annotation-geometry-types | polygon | ![annotation-geometry:polygon](svgs/image-annotation-geometry_polygon.svg "annotation-geometry:polygon") | A detailed outline was drawn around objects of interest |
|  | whole-image | ![annotation-geometry:whole-image](svgs/image-annotation-geometry_whole-image.svg "annotation-geometry:whole-image") | The entire image was annotated without defining a region of interest |
|  | bounding-box | ![annotation-geometry:bounding-box](svgs/image-annotation-geometry_bounding-box.svg "annotation-geometry:bounding-box") | A bounding box was drawn around objects of interest |
|  | single-pixel | ![annotation-geometry:single-pixel](svgs/image-annotation-geometry_single-pixel.svg "annotation-geometry:single-pixel") | Single points in the images were marked |
| image-annotation-creator-types | non-expert | ![annotation-creator:non-expert](svgs/image-annotation-creator_non-expert.svg "annotation-creator:non-expert") | Annotations were created by trained, yet non-expert persons |
|  | AI | ![annotation-creator:AI](svgs/image-annotation-creator_AI.svg "annotation-creator:AI") | Annotations were created by an artifical intelligence method |
|  | expert | ![annotation-creator:expert](svgs/image-annotation-creator_expert.svg "annotation-creator:expert") | Annotations were created by trained experts |
|  | crowd-sourced | ![annotation-creator:crowd-sourced](svgs/image-annotation-creator_crowd-sourced.svg "annotation-creator:crowd-sourced") | Annotations were created by the general public |
| image-annotation-label-types | geology | ![annotation-labels:geology](svgs/image-annotation-labels_geology.svg "annotation-labels:geology") | Geological structures were annotated |
|  | biology | ![annotation-labels:biology](svgs/image-annotation-labels_biology.svg "annotation-labels:biology") | Fauna and traces of life were annotated |
|  | operation | ![annotation-labels:operation](svgs/image-annotation-labels_operation.svg "annotation-labels:operation") | Gear oeprations were annotated |
|  | garbage | ![annotation-labels:garbage](svgs/image-annotation-labels_garbage.svg "annotation-labels:garbage") | Garbage was annotated |
