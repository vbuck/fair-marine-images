# iFDO capture section
The iFDO capture is one section of the complete [iFDO file](iFDO-overview.md). See its description to learn about its file formats, parts and sections.  All fields in this section are optional!

## Motivation
Information on how image data was captured can be crucial to understand information extracted from the images. It is thus highly recommended to enrich all iFDOs with capture information. The potential metadata in the iFDO capture fields is expected to grow with time, as additional (marine) imaging domains make use of this concept. Anyhow, below you find a pool of iFDO capture fields which are highly recommened to be added to your iFDO. Only with these fields populated will your dataset shine in a marine data portal!

## File format
All iFDO capture fields shall be stored alongside the core metadata in your iFDO file! It does not take up a specific section of the file, rather the values are intermixed into the image-set-header and image-set-items section!

# iFDO capture fields

## ... with restricted values (all to be given as strings)

| Field | Allowed Values | Comment |
| ----- | -------------- | ------- |
| image-acquisition | photo, video, slide | photo: still images, video: moving images, slide: microscopy images / slide scans |
| image-quality | raw, processed, product | raw: straight from the sensor, processed: QA/QC'd, product: image data ready for interpretation | 
| image-deployment | mapping, stationary, survey, exploration, experiment, sampling | mapping: planned path execution along 2-3 spatial axes, stationary: fixed spatial position, survey: planned path execution along free path, exploration: unplanned path execution, experiment: observation of manipulated environment, sampling: ex-situ imaging of samples taken by other method |
| image-navigation | satellite, beacon, transponder, reconstructed | satellite: GPS/Galileo etc., beacon: USBL etc., transponder: LBL etc., reconstructed: position estimated from other measures like cable length and course over ground |
| image-scale-reference | 3D camera, calibrated camera, laser marker, optical flow | 3D camera: the imaging system provides scale directly, calibrated camera: image data and additional external data like object distance provide scale together, laser marker: scale information is embedded in the visual data, optical flow: scale is computed from the relative movement of the images and the camera navigation data |
| image-illumination | sunlight, artificial light, mixed light | sunlight: the scene is only illuminated by the sun, artificial light: the scene is only illuminated by artificial light, mixed light: both sunlight and artificial light illuminate the scene |
| image-pixel-magnitude | km, hm, dam, m, cm, mm, µm | average size of one pixel of an image |
| image-marine-zone | seafloor, water column, sea surface, atmosphere, laboratory | seafloor: images taken in/on/right above the seafloor, water column: images taken in the free water without the seafloor or the sea surface in sight, sea surface: images taken right below the sea surface, atmosphere: images taken outside of the water, laboratory: images taken ex-situ |
| image-spectral-resolution | grayscale, rgb, multi-spectral, hyper-spectral | grayscale: single channel imagery, rgb: three channel imagery, multi-spectral: 4-10 channel imagery, hyper-spectral: 10+ channel imagery |
| image-capture-mode | timer,manual,mixed | whether the time points of image capture were systematic, human-truggered or both |
| image-fauna-attraction | none, baited, light | Allowed: none, baited, light |

By limiting these fields to restricted values, it is possible to classify and filter image data sets in data portals and to visualize data characteristics. See the [iFDO capture icon overview](https://gitlab.hzdr.de/datahub/marehub/ag-videosimages/fair-marine-images/-/blob/master/docs/ifdos/iFDO-capture-icons.md) for more details 


## ... with free values

| Field | Format | Comment |
| ----- | -------------- | ------- |
| image-area-square-meter | float | The footprint of the entire image in square meters |
| image-meters-above-ground | float | Distance of the camera to the seafloor in meters |
| image-acquisition-settings | dict | All the information that is recorded by the camera in the EXIF, IPTC etc. As a dict. Includes ISO, aperture, etc. |
| image-camera-yaw-degrees | float | Camera view yaw angle. Rotation of camera coordinates (x,y,z = top, right, line of sight) with respect to NED coordinates (x,y,z = north,east,down) in accordance with the yaw,pitch,roll rotation order convention: 1. yaw around z, 2. pitch around rotated y, 3. roll around rotated x. Rotation directions according to \'right-hand rule\'. I.e. for yaw,pitch,roll = 0,0,0 camera is facing downward with top side towards north. |
| image-camera-pitch-degrees | float | Camera view pitch angle. Rotation of camera coordinates (x,y,z = top, right, line of sight) with respect to NED coordinates (x,y,z = north,east,down) in accordance with the yaw,pitch,roll rotation order convention: 1. yaw around z, 2. pitch around rotated y, 3. roll around rotated x. Rotation directions according to \'right-hand rule\'. I.e. for yaw,pitch,roll = 0,0,0 camera is facing downward with top side towards north. |
| image-camera-roll-degrees | float | Camera view roll angle. Rotation of camera coordinates (x,y,z = top, right, line of sight) with respect to NED coordinates (x,y,z = north,east,down) in accordance with the yaw,pitch,roll rotation order convention: 1. yaw around z, 2. pitch around rotated y, 3. roll around rotated x. Rotation directions according to \'right-hand rule\'. I.e. for yaw,pitch,roll = 0,0,0 camera is facing downward with top side towards north. |
| image-overlap-fraction | float | The average overlap of two consecutive images i and j as the area images in both of the images (A_i * A_j) divided by the total area images by the two images (A_i + A_j - A_i * A_j): f = A_i * A_j / (A_i + A_j - A_i * A_j) -> 0 if no overlap. 1 if complete overlap |
| image-datetime-format | string | A date time format string in Python notation (e.g. %Y-%m-%d %H:%M:%S.%f) to specify a different date format used throughout the iFDO file. The assumed default is the one in brackets. Make sure to reach second-accuracy with your date times! |
| image-camera-pose | dict | Information required to specify camera pose. For details on subfields see rows below. |
| image-camera-pose:pose-utm-zone | string | The UTM zone number | 
| image-camera-pose:pose-utm-epsg | string | The EPSG code of the UTM zone |
| image-camera-pose:pose-utm-east-north-up-meters | [float,float,float] | The position of the camera center in UTM coordinates. |
| image-camera-pose:pose-absolute-orientation-utm-matrix | list | 3x3 row-major float rotation matrix that transforms a direction in camera coordinates (x,y,z = right,down,line of sight) into a direction in UTM coordinates (x,y,z = easting,northing,up)} |
| image-camera-housing-viewport | dict | Information on the camera pressure housing viewport (the glass). For details on subfields see rows below. |
| image-camera-housing-viewport:viewport-type | string | e.g.: flatport, domeport, other |
| image-camera-housing-viewport:viewport-optical-density | float | Unit-less optical density number (1.0=vacuum) |
| image-camera-housing-viewport:viewport-thickness-millimeter | float | Thickness of viewport in millimeters |
| image-camera-housing-viewport:viewport-extra-description | text | A textual description of the viewport used |
| image-flatport-parameters | dict | Information required to specify the characteristics of a flatport camera housing. For details on subfields see rows below. |
| image-flatport-parameters:flatport-lens-port-distance-millimeter | float | The distance between the front of the camera lens and the inner side of the housing viewport in millimeters. |
| image-flatport-parameters:flatport-interface-normal-direction | [float, float, float] | 3D direction vector to specify how the view direction of the lens intersects with the viewport (unit-less, (0,0,1) is "aligned") |
| image-flatport-parameters:flatport-extra-description | text | A textual description of the flatport used |
| image-domeport-parameters | dict | Information required to specify the characteristics of a domeport camera housing. For details on subfields see rows below. |
| image-domeport-parameters:domeport-outer-radius-millimeter | float | Outer radius of the domeport - the part that has contact with the water. |
| image-domeport-parameters:domeport-decentering-offset-xyz-millimeter | [float,float,float] | 3D offset vector of the camera center from the domeport center in millimeters |
| image-domeport-parameters:domeport-extra-description | text | A textual description of the domeport used |
| image-camera-calibration-model | dict | Information required to specify the camera calibration model. For details on the subfields see rows below. |
| image-camera-calibration-model:calibration-model-type | string | e.g.: rectilinear air, rectilinear water, fisheye air, fisheye water, other |
| image-camera-calibration-model:calibration-focal-length-xy-pixel | [float, float] | 2D focal length in pixels |
| image-camera-calibration-model:calibration-principal-point-xy-pixel | [float,float] | 2D principal point of the calibration in pixels (top left pixel center is 0,0, x right, y down) |
| image-camera-calibration-model:calibration-distortion-coefficients | list | rectilinear: k1, k2, p1, p2, k3, k4, k5, k6, fisheye: k1, k2, k3, k4 |
| image-camera-calibration-model:calibration-approximate-field-of-view-water-xy-degree | [float, float] | Proxy for pixel to meter conversion, and as backup |
| image-camera-calibration-model:calibration-model-extra-description | text | Explain model, or if lens parameters are in mm rather than in pixel |
| image-photometric-calibration | dict | Information required to specify the photometric calibration. For details on the subfields see rows below. |
| image-photometric-calibration:photometric-sequence-white-balancing | text | A text on how white-balancing was done. |
| image-photometric-calibration:photometric-exposure-factor-RGB | [float, float, float] | RGB factors applied to this image, product of ISO, exposure time, relative white balance |
| image-photometric-calibration:photometric-sequence-illumination-type | string | e.g. "constant artificial", "globally adapted artificial", "individually varying light sources", "sunlight", "mixed") |
| image-photometric-calibration:photometric-sequence-illumination-description | text | A text on how the image sequence was illuminated |
| image-photometric-calibration:photometric-illumination-factor-RGB | [float, float, float] | RGB factors applied to artificial lights for this image |
| image-photometric-calibration:photometric-water-properties-description | text | A text describing the photometric properties of the water within which the images were capture |
| image-objective | text | A general translation of the aims and objectives of the study, as they pertain to biology and method scope. This should define the primary and secondary data to be measured and to what precision. |
| image-target-environment | text | A description, delineation, and definition of the habitat or environment of study, including boundaries of such |
| image-target-timescale | text | A description, delineation, and definition of the period, interval or temporal environment of the study. |
| image-spatial-contraints | text | A description / definition of the spatial extent of the study area (inside which the photographs were captured), including boundaries and reasons for constraints (e.g. scientific, practical) |
| image-temporal-constraints | text | A description / definition of the temporal extent, including boundaries and reasons for constraints (e.g. scientific, practical) |
| image-time-synchronisation | text | Synchronisation procedure and determined time offsets between camera recording values and UTC |
| image-item-identification-scheme | text | How the images file names are constructed. Should be like this `<project>_<event>_<sensor>_<date>_<time>.<ext>` |
| image-curation-protocol | text | A description of the image and metadata curation steps and results |
| ... and many more to come |  | Please suggest more by reporting an issue or creating a merge request! | 

# Examples
[iFDO capture example](https://gitlab.hzdr.de/datahub/marehub/ag-videosimages/fair-marine-images/-/blob/master/resources/SO268-1_021-1_GMR_CAM-23_example-iFDO_capture.yaml)
