# Mapping iFDO fields to other standards
Fields of the [iFDO](iFDO-overview.md) image metadata standard are inspired by other metadata standards. iFDOs are designed to facilitate mapping between all relevant image metadata standards.

## iFDO core fields (required)
| [iFDOs](iFDO-overview.md) | [DarwinCore](https://dwc.tdwg.org/terms) | [Pangaea](https://wiki.pangaea.de/wiki/Data_submission) | [schema.org](https://schema.org/docs/schemas.html) | [OBIS](https://obis.org/manual/darwincore/) | [MEDIN/BODC](https://www.medin.org.uk/sites/medin/files/documents/MEDIN_Schema_Documentation3_1_brief.pdf) | [Audubon](https://ac.tdwg.org/termlist) | [SMarTaR-ID](https://doi.org/10.1371/journal.pone.0218904) | 
| - | - | - | - | - | - | - | - |
| image-set-name | datasetName | Dataset:Title | name | datasetName | Resource title |  |  |
| image-set-uuid | datasetID |  | identifier | datasetID | Unique resource identifier |  |  |
| image-set-handle |  | DOI (after publication) |  |  |  | ac:accessURI |  |
| image-datetime | eventDate | DATE/TIME (1599) | dateCreated | eventDate | Temporal reference  | ac:startTime | dateIdentified |
| image-latitude | decimalLatitude | LATITUDE (1600) |  | decimalLatitude |  | dwc:decimalLatitude | decimalLatitude |
| image-longitude | decimalLongitude | LONGITUDE (1601) |  | decimalLongitude |  | dwc:decimalLongitude  | decimalLongitude |
| image-depth | verbatimDepth | DEPTH, water [m] (1619) |  | minimumDepthInMeters / maximumDepthInMeters | Vertical extent information | dwc:minimumDepthInMeters / dwc:maximumDepthInMeters | minimumDepthInMeters / maximumDepthInMeters |
| image-altitude | verbatimElevation | ALTITUDE [m] (4607) |  |  |  |  |  |
| image-coordinate-reference-system | verbatimSRS |  |  | verbatimSRS | Spatial reference system |  |  |
| image-coordinate-uncertainty-meters | coordinatePrecision | Coordinate uncertainty (170981) |  | coordinateUncertaintyInMeters |  | dwc:pointRadiusSpatialFit |  |
| image-project |  | Dataset:Project | isPartOf |  |  |  |  |
| image-context |  |  |  |  |  |  |  |
| image-event |  | Event |  |  |  |  |  |
| image-platform |  | Event:Platform |  |  |  |  |  |
| image-sensor |  | Event:Sensor |  |  |  | ac:captureDevice? |  |
| image-uuid | eventID |  | identifier | eventID |  |  |  |
| image-hash-sha256 |  |  |  |  |  | ac:hashValue |  |
| image-filename |  | File name (25541) | name |  |  |  |  |
| image-pi |  | Dataset:PI |  |  | Originator |  |  |
| image-creators | rightsHolder | Dataset:Authors | creator | rightsHolder | Responsible party | dc::creator |  |
| image-license | license | Dataset:License | license | license | Limitations on public access/ Conditions applying for access and use |  |  |
| image-copyright |  |  |  |  |  |  |  |
| image-abstract |  | Dataset:Abstract |  |  | Resource abstract |  |  |

## iFDO capture fields (recommended)
| [iFDOs](iFDO-overview.md) | [DarwinCore](https://dwc.tdwg.org/terms) | [Pangaea](https://wiki.pangaea.de/wiki/Data_submission) | [schema.org](https://schema.org/docs/schemas.html) | [OBIS](https://obis.org/manual/darwincore/) | [MEDIN/BODC](https://www.medin.org.uk/sites/medin/files/documents/MEDIN_Schema_Documentation3_1_brief.pdf) | [Audubon](https://ac.tdwg.org/termlist) | [SMarTaR-ID](https://doi.org/10.1371/journal.pone.0218904) | 
| - | - | - | - | - | - | - | - |
| image-acquisition | type |  | ImageObject / VideoObject | type | Resource type / Data format | dc:format? |  |
| image-quality |  |  |  |  |  |  |  |
| image-deployment |  |  |  |  |  |  |  |
| image-navigation |  |  |  |  |  |  |  |
| image-scale-reference |  |  |  |  |  |  |  |
| image-illumination |  |  |  |  |  |  |  |
| image-resolution |  |  |  |  |  |  |  |
| image-marine-zone |  |  |  |  |  |  |  |
| image-spectral-resolution |  |  |  |  |  |  |  |
| image-capture-mode |  |  |  |  |  |  |  |
| image-area-square-meter |  |  |  |  |  |  |  |
| image-pixel-per-millimeter |  | Image resolution (172673) |  |  | Spatial resolution |  |  |
| image-meters-above-ground |  | HEIGHT above ground [m] (56349) |  |  | Distance  |  |  |
| image-acquisition-settings |  |  |  |  | Lineage(?) |  |  |
| image-camera-yaw-degrees |  |  |  |  |  |  |  |
| image-camera-pitch-degrees |  |  |  |  |  |  |  |
| image-camera-roll-degrees |  |  |  |  |  |  |  |
| image-camera-pose |  |  |  |  |  |  |  |
| image-camera-housing-viewport |  |  |  |  |  |  |  |
| image-camera-flatport-parameters |  |  |  |  |  |  |  |
| image-camera-domeport-parameters |  |  |  |  |  |  |  |
| image-camera-calibration-model |  |  |  |  |  |  |  |
| image-camera-photometric-calibration |  |  |  |  |  |  |  |
| image-objective |  |  |  |  |  |  |  |
| image-target-environment |  |  |  |  |  | dwc:locationRemarks? |  |
| image-target-timescale |  |  |  |  |  |  |  |
| image-spatial-constraints | footprintWKT? |  |  |  |  | dwc:footprintWKT? | locality? |
| image-temporal-constraints |  |  |  |  |  | dcterms:temporal? |  |
| image-reference-calibration |  |  |  |  |  |  |  |
| image-time-synchronization |  |  |  |  |  |  |  |
| image-item-identification-scheme |  |  |  |  |  |  |  |
| image-curation-protocol |  |  |  |  |  |  |  |

## iFDO content fields (optional)
| [iFDOs](iFDO-overview.md) | [DarwinCore](https://dwc.tdwg.org/terms) | [Pangaea](https://wiki.pangaea.de/wiki/Data_submission) | [schema.org](https://schema.org/docs/schemas.html) | [OBIS](https://obis.org/manual/darwincore/) | [MEDIN/BODC](https://www.medin.org.uk/sites/medin/files/documents/MEDIN_Schema_Documentation3_1_brief.pdf) | [Audubon](https://ac.tdwg.org/termlist) | [SMarTaR-ID](https://doi.org/10.1371/journal.pone.0218904) | 
| - | - | - | - | - | - | - | - |
| image-entropy |  |  |  |  |  |  |  |
| image-particle-count |  |  |  |  |  |  |  |
| image-average-color |  |  |  |  |  |  |  |
| image-mpeg7-... |  |  |  |  |  |  |  |
| image-annotations |  |  |  |  |  | ac:subjectOrientation? | |
| image-annotation-labels | taxonID? |  |  |  |  | dwc:scientificName? | scientificName / scientificNameID |
| image-annotation-creators | identifiedBy? |  |  |  |  | dwc:identifiedBy? | identifiedBy |


## Disclaimer
Vocabularies, repositories or other sources that do not enforce structured meta data or data (like Zenodo or DRYAD) are not considered here as they do not lead to FAIRness of data. Similarly, all projects, workflows, etc. that rely on these sources are omitted as well.

Other sources not (yet?) included here:
- http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.733.1984&rep=rep1&type=pdf
- https://www.nature.com/articles/sdata2018181
