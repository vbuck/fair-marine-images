# iFDO core
The iFDO core is one section of the complete [iFDO file](iFDO-overview.md).  See its description to learn about its file formats, parts and sections. All fields in this section are mandatory!

## Header information in the `image-set-header` part
These three `image-set-header` values have to exist and cannot be superseded by values of the `image-set-items`. Fields from the `image-set-items` part (below) may exist in the header as default values for the entire image set. Bold text shows suggested best-practices.

| Field | Format / Values / Unit | Comment |
| ----- | ---------------------- | ------- |
| image-set-name | string | A unique name for the image set, should include `<project>, <event>, <sensor>` and purpose |
| image-set-uuid | UUID | A UUID (**version 4 - random**) for the entire image set |
| image-set-handle | string | A Handle URL (using the UUID?) to point to the landing page of the data set |
| image-set-ifdo-version | string | The semantic version information of the iFDO standard used. The default is v1.0.0 |

In yaml, the entire `image-set-header` part is one dictionary.

## Image item information in the `image-set-items` part

| Field | Format | Comment |
| ----- | ---------------------- | ------- |
| image-datetime | string | The fully-qualified ISO8601 UTC time of image acquisition (or start time of a video). E.g.: %Y-%m-%d %H:%M:%S.%f (in Python). You *may* specify a different date format using the optional iFDO capture field `image-datetime-format` |
| image-latitude | float | Y-coordinate of the camera center in decimal degrees: D.DDDDDDD (use at least seven significant digits that is ca. 1cm resolution) |
| image-longitude | float | X-coordinate of the camera center in decimal degrees: D.DDDDDDD (use at least seven significant digits that is ca. 1cm resolution) |
| image-depth | float | Z-coordinate of camera center in meters. *Use this when camera is below water, then it has positive values.* |
| image-altitude | float | Z-coordinate of camera center in meters. *Use this when camera is above water, then it has positive values.* You can also use image-depth with negative values instead! |
| image-coordinate-reference-system | string | The coordinate reference system, e.g. **EPSG:4326** |
| image-coordinate-uncertainty-meters | float | The average/static uncertainty of coordinates in this dataset, given in meters. Computed e.g. as the standard deviation of coordinate corrections during smoothing / splining. |
| image-context | string | The high-level "umbrella" project |
| image-project | string | The lower-level / specific expedition or cruise or experiment or ... |
| image-event | string | One event of a project or expedition or cruise or experiment or ... |
| image-platform | string | Platform URN or Equipment Git ID or Handle URL |
| image-sensor | string | Sensor URN or Equipment Git ID or Handle URL |
| image-uuid | UUID | UUID (**version 4 - random**) for the image file (still or moving) |
| image-hash-sha256 | string | An SHA256 hash to represent the whole file (including UUID in metadata!) to verify integrity on disk |
| image-pi | dict | Information to identify the principal investigator. See details below |
| image-pi:name | string | Full name of principal investigator |
| image-pi:orcid | string | ORCID of principal investigator |
| image-creators | list | A list containing dicts for all creators containing: {orcid:..., name:...} |
| image-license | string | License to use the data (should be FAIR, e.g. **CC-BY** or CC-0) |
| image-copyright | text | Copyright sentence / contact person or office |
| image-abstract | text | 500 - 2000 characters describing what, when, where, why and how the data was collected. Includes general information on the event (aka station, experiment), e.g. overlap between images/frames, parameters on platform movement, aims, purpose of image capture etc. |

In yaml, the `image-set-items` part is one dictionary. The keys in that dictionary are the filenames of the items in the image set. Each item, indexed by the `image-filename`, is itself a list ob dictionaries. For videos, the first entry of the list contains the default fields for the entire video. Do not repeat static metadata for each second of a video to avoid repetition. Every subsequent entry contains specifications that supersede the default values for one specific time point of the video. For photos (still images) this list has only one entry, containing the fields tabled above or it is no list at all but rather the single dictionary for this photo.

## UUIDs and hashes
### UUID
Making data FAIR requires - amongst other things - that data is assigned a persistent identifier (PID). Many such PID systems exist, and usually they are based on the handle system (like DOIs for example) and alpha-numerical IDs that are globally unique. For images, we chose to use UUIDs (*Universally Unique Identifiers*), more precisely UUID type 4: random. These UUIDs can be created by anyone and as there are ca 21^36 possible UUID4s making it almost impossible that the same one is created more than once. This UUID is the alphanumerical identifier that has to be assigned to each image and to each image set. That means that the UUID for an image item (a photo or video) *has to become part of the file itself!* You need to write it into the image file's metadata header. How this can be done depends on the image file format you chose but in general the two magnificent software tools *exiftool* anf *ffmpeg* are the solution. In case you are using the MarIQT software, these tools are used under the hood.

### Hash
We use hashes to document the integrity of the image files. A hash is like a fingerprint of the file as it is computed from the file's byte content. If you like, it is a massive compression of the file's data into a short, cryptic text (ca. 32 characters) but unlike a zip file there is no way to uncompress the file from the hash. Using hashes allows us to make sure that a file has not gone corrupt or that a particular file is actually the version we are interested in. Checking the integrity of a file with hashes requires, that the byte content does not change. It is therefore absolutely essential, that the UUID is written to the image file's metadata header **before** the hash for that file is computed!

# Example:
[iFDO core example](https://gitlab.hzdr.de/datahub/marehub/ag-videosimages/fair-marine-images/-/blob/master/resources/SO268-1_021-1_GMR_CAM-23_example-iFDO_core.yaml)
