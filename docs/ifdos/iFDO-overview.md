# Introduction
Achieving FAIRness and Openness of (marine) image data requires structured and standardised metadata on the image data itself and the visual and semantic image data content. This metadata shall be provided in the form of FAIR digital objects (FDOs). These documentation pages describes how FDOs for images (aka iFDOs) shall be structured. If you want, an iFDO is a human and machine-readable file format for an entire image set, except that it does not contain the actual image data, only references to it through persistent identifiers!
iFDOs consist of various metadata fields. Some are required, some are recommended, some are optional. You will only achieve FAIRness of your image data with the required iFDO core fields populated. You will only gain visibility and credit for your image data with the recommended capture fields populated. And you will only have awesome image data in case you also populate the content fields. As a bonus you can add your own domain-specific optional fields.

# iFDO - image FAIR Digital Object
Marine image data collections need a set of standardized metadata to achieve FAIRness of the data for open publication. An entire image set (e.g. deployment, station, dive, mission) requires information on the ownership and allowed usage of the collection. Numerical metadata is required for each image on its acquisition position. It is recommended to provide further optional metadata based on the imaging use case. The iFDO standard defines a format to structure such metadata. It was developed by the MareHub working group and the Marine Imaging Community. The future if iFDOs is described in the [roadmap](../roadmap.md). The development of the format is decumented on the [versions](../versions.md) page.

### Quick facts:
- iFDOs are made for photos ([still images](http://purl.org/dc/dcmitype/StillImage)) and videos ([moving images](http://purl.org/dc/dcmitype/MovingImage))
- iFDOs consist of an `image-set-header` an `image-set-items` [part](#ifdo-parts)
- iFDOs group metadata fields in three [sections](#ifdo-sections): iFDO core, iFDO capture and iFDO content
- iFDO [core](iFDO-core.md) fields are mandatory
- iFDO [capture](iFDO-capture.md) and [content](iFDO-content.md) fields are optional but recommended
- iFDO fields can be [mapped](iFDO_vocabulary-term-mapping.md) to many other metadata standards
- iFDOs make image data [FAIR](../FAIR-marine-images.md) without requiring them to be open, access-restriction remains possible
- iFDO field names use [kebab-case](https://en.wikipedia.org/wiki/Letter_case#Kebab_case)
- The iFDO documentation is written with Python implementation in mind

### iFDO files
All image metadata shall be stored in one image FAIR digital object (iFDO) file. This file shall contain all iFDO metadata fields. The file should be human and machine-readable, hence *.yaml format is recommended. The file name should be unique, we recommend: `<image-project>_<image-event>_<image-sensor>`_iFDO.yaml. iFDO files have to be well-formatted according to the documentation on these pages. They are light-weight and can evolve along their lifecycle.
![iFDO lifecycle](../assets/iFDO_lifecycle.png) 

### iFDO parts
An iFDO file consists of two parts: the `image-set-header` part and the `image-set-items` part. The header part contains default values for all items. The items part contains all values that deviate from the default values for this specific image item. The `image-set-header` part is a dictionary within which each metadata field is referenced by a key term such as `image-set-uuid`. The `image-set-items` part is also a dictionary, where the keys are the filenames of the images (i.e. photos or videos). All metadata of an image-item is provided in a list. For still images (aka photos) this list has only one object as an entry. For videos, the first entry of the list contains an object of those metadata fields that are defaults for the entire video. All subsequent list entries correspond to specifications of the metadata for one given timepoint of the video.

### iFDO sections
The iFDO standard defines three different section that cover different aspects of FAIRness and target increasing usability of the image metadata. The [iFDO core](iFDO-core.md) section contains the required metadata that makes image data sets FAIR. The [iFDO capture](iFDO-capture.md) section contains metadata fields that describe how the image data was created. The [iFDO content](iFDO-content.md) section describes what is going on in the image data in terms of information or annotations.
![iFDO sections](../assets/iFDO_sections.png)

### Mapping to other image metadata standards
The iFDOs standard aspires to be the most complete standard that allows to bridge between existing standards (like DublinCore, Audubon, SmartarID, PDS4) while filling gaps among those. We maintain a [term mapping](iFDO_vocabulary-term-mapping.md) of vocabulary terms to other standards.


