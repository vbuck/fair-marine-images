## FAIR marine images
Data management for images is hard. Much harder than most other data domains in research. The data volume is massive (> 1TB per deployment) and the data is inherently unstructured and thus not directly comprehensible for humans or machines. So of course, making marine imagery FAIR is also a major challenge but particularly worthwile. It will i) simplify working and researching with image data by providing well-structured information and formats, ii) it will simplify software development by providing community-driven interfaces and iii) it will allow researchers to gain credit for imaging efforts by making image data publicly visible - for collaboration, inspiration and to advertise one's research.

Curiously, FAIR does not necessarily mean open. So while - in theory - your data can be FAIR it can still be very much locked up. This would go against the principles and vision outlined above, so in the context of the MareHub Ag Videos/Images FAIR also always means open. Period.
(Well 99% period: we accept that some research requires obfuscating parts of the data or metadata. This includes personal rights, safety concerns and maybe others. In case you are in doubt you probably belong to the 99%. Although these exceptions exist, you will certainly know whether your research falls amongst the 1%).

## FAIRness of marine images
What FAIR means and is and how to become FAIR is an active field of research. There are vast amounts of information available: e.g. by the [EOSC](https://eosc-portal.eu/) (European Open Science Could) or the [RDA](https://rd-alliance.org/) (Research Data Alliance). Their working groups produce fantastic implementation guidelines for users, managers, governing agencies etc. including ones on _Metrics to assess FAIRness_ (by EOSC: [Recommendations on FAIR metrics](https://doi.org/10.2777/70791), by RDA: [FAIR Data Maturity Model](https://doi.org/10.15497/rda00050)).

The following table applies these metrics to marine images to track our status at achieving our goal: to make marine images FAIR.

| [FAIR principle](https://www.go-fair.org/fair-principles/) | [FAIR Metrics ID](https://doi.org/10.2777/70791) | Indicator | Priority | Marine images |
| - | - | - | - | - |
| F1 | RDA-F1-01M | Metadata is identified by a persistend identifier | Essential | [ ] Handle
| F1 | RDA-F1-01D | Data is identified by a persistent identifier | Essential | [ ] Handle |
 | F1 | RDA-F1-02M | Metadata is identified by a globally unique identifier | Essential | [x] UUID |
 | F1 | RDA-F1-02D | Data is identified by a globally unique identifier | Essential | [x] UUID |
 | F2 | RDA-F2-01M | Rich metadata is provided to allow discovery | Essential | [x] [iFDO](ifdos/iFDO-overview.md) |
 | F3 | RDA-F3-01M | Metadata includes the indentifier for the data | Essential | [x] iFDO |
 | F4 | RDA-F4-01M | Metadata is offered in such a way that it can be harvested and indexed | Essential | [ ] OSIS-API |
 | A1 | RDA-A1-01M | Metadata contains information to enable the user to get access to the data | Important | [x] iFDO |
 | A1 | RDA-A1-02M | Metadata can be accessed manually | Essential | [x] [OSIS-www](https://osis.geomar.de/app) |
 | A1 | RDA-A1-02D | Data can be accessed manually | Essential | [x] Elements |
 | A1 | RDA-A1-03M | Metadata identifier resolves to a metadata record | Essential | [x] iFDO |
 | A1 | RDA-A1-03D | Data identifiert resolves to a digital object | Essential | [x] iFDO |
 | A1 | RDA-A1-04M | Metadata is acccessed through standardised protocol | Essential | [x] https |
 | A1 | RDA-A1-04D | Data is accessible through standardised protocol | Essential | [x] https |
 | A1 | RDA-A1-05D | Data can be accessed automatically | Important | [x] Elements-API |
 | A1.1 | RDA-A1.1-01M | Metadata is accessible through a free access protocol | Essential | [x] https |
 | A1.1 | RDA-A1.1-01D | Data is accessible through a free access protocol | Important | [x] https |
 | A1.2 | RDA-A1.2-01D | Data is accessible through an access protocol that supports authentication and authorisation | Useful | [ ] _No_ |
 | A2 | RDA-A2-01M | Metadata is guaranteed to remain available after data is no longer available | Essential | [x] Yes |
 | I1 | RDA-I1-01M | Metadata uses knowledge representation expressed in standardised format | Important | [x] [iFDO](ifdos/iFDO-overview.md) |
 | I1 | RDA-I1-01D | Data uses knowledge representation expressed in standardised format | Important | [x] jpg, tif, png |
 | I1 | RDA-I1-02M | Metadata uses machine-understandable knowledge representation | Important | [x] FDO |
 | I1 | RDA-I1-02D | Data uses machine-understandable knowledge representation | Important | [x] jpg, tif, png |
 | I2 | RDA-I2-01M | Metadata uses FAIR-compliant vocabularies | Important | [ ] MVP Git? |
 | I2 | RDA-I2-01D | Data uses FAIR-compliant vocabularies | Useful | [x] MVP Git |
 | I3 | RDA-I3-01M | Metadata includes references to other metadata | Important | [x] Orcid, URN |
 | I3 | RDA-I3-01D | Data includes references to other data | Useful | [ ] _No_ |
 | I3 | RDA-I3-02M | Metadata includes refereces to other data | Useful | [ ] _No_ |
 | I3 | RDA-I3-02D | Data includes qualified references to other data | Useful | [ ] _No_ |
 | I3 | RDA-I3-03M | Metadata includes qualified references to other metadata | Important | [ ] **No** | 
 | I3 | RDA-I3-04M | Metadata includes qualified references to other data | Useful | [ ] _No_ |
 | R1 | RDA-R1-01M | Plurality of accurate and relevant attributes are provided to allow reuse | Essential | [x] iFDO |
 | R1.1 | RDA-R1.1-01M | Metadata includes information about the license under which the data can be reused | Essential | [x] iFDO |
 | R1.1 | RDA-R1.1-02M | Metadata refers to a standard reuse license | Important | [x] iFDO |
 | R1.1 | RDA-R1.1-03M | Metadata refers to a machine-understandable reuse license | Important | [x] iFDO |
 | R1.2 | RDA-R1.2-01M | Metadata includes provenance information according to community-specific standards | Important | [ ] **No** |
 | R1.2 | RDA-R1.2-02M | Metadata includes provenance information according to a cross-community language | Useful | [ ] _No_ |
 | R1.3 | RDA-R1.3-01M | Metadata complies with a community standard | Essential | [x] MVP |
 | R1.3 | RDA-R1.3-01D | Data complies with a communty standard | Essential | [x] MVP |
 | R1.3 | RDA-R1.3-03M | Metadata is expressed in compliance with a machine-understanable community standard | Essential | [x] MVP |
 | R1.3 | RDA-R1.3-02D | Data is expressed incompliance with a machine-understandable community standard | Important | **No** |

_(as of April 2021 - while there are still challenges to overcome, we are close to achieving FAIRness - at least conceptually)_
