# Current version - v1.1.0
The state of the repository and documentation represents the current state-of-the-art of the iFDO standard. The current version as documented here is v1.1.0. The version of an iFDO can be specified by the `image-set-ifdo-version` field in the `image-set-header` part.

# Changelog

### v1.0.0 -> v1.1.0
- Added the required `image-set-ifdo-version` field as an `image-set-header`-only field of the iFDO core section
- Dropped the optional `image-pixel-per-millimeter` field from the iFDO capture section due to redundancy with `image-area-square-meter` field (you can keep using it if you like but tools might not understand it anymore)
- Renamed `image-resolution` in iFDO capture section to `image-pixel-magnitude` to avoid ambiguity
- Added the optional `image-datetime-format` field to specify non-standard date formats
- Added the `image-annotations:labels:created-at` field as an ISO8601 datetime of an annotation
- Corrected wrong data types in `image-camera-pose` subfields. 
- Modified the description texts (on using iFDO usage for video, image pixel coordinates, annotation confidence, )
- specified field data types consistenly
- Added a roadmap of the iFDO development

