> **Preamble:** We strive to make marine image data [FAIR](docs/FAIR-marine-images.md). We maintain [metadata profiles](docs/ifdos/iFDO-overview.md) to establish a common language of marine imagery, we develop best-practice [operating procedures](docs/sops/sop-overview.md) for handling marine images and we develop [software tools](https://gitlab.hzdr.de/datahub/marehub/ag-videosimages/) to apply the vocabulary and procedures to marine imagery.

# Overview
This repository contains documentation on how to make (marine) image data FAIR (and open). You have three options to browse the information:
- https://datahub.pages.hzdr.de/marehub/ag-videosimages/fair-marine-images/ (pretty, protected, up-to-date)
- https://marine-imaging.com/fair (pretty, public, not up-to-date)
- use this repository (not pretty, but public and up-to-date)

# Resources for download
-   [Excel sheet](resources/iFDO.xlsx) to collect metadata information for [iFDOs](docs/ifdos/iFDO-overview.md)
-   [Example iFDO core](resources/SO268-1_021-1_GMR_CAM-23_example-iFDO_core.yaml)
-   [Example iFDO capture](resources/SO268-1_021-1_GMR_CAM-23_example-iFDO_capture.yaml)
-   [Example iFDO content](resources/SO268-1_021-1_GMR_CAM-23_example-iFDO_content.yaml)
-   [Allowed terms](resources/MareHub_AGVI_iFDO_capture-vocabulary.yaml) of [iFDO capture](docs/ifdos/iFDO-capture.md) fields
